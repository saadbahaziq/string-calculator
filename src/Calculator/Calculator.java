package Calculator;

public class Calculator {

	public static Integer add(String text) {
		if(text.isEmpty()) {
			return 0;
		}
		else if(text.contains(",")){
			String[] tokens = text.split(",|\n|\\;|\\.");
			int sum = 0;
			for(int i=0; i< tokens.length; i++) {
				sum += toInt(tokens[i]);
			}
			return sum;
		}
		else {
			return toInt(text);
		}
	}
	private static int toInt(String s) throws NumberFormatException {
		return Integer.parseInt(s);
	}

}
